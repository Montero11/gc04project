package com.example.goenjoy.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import com.example.goenjoy.converter.DateConverter;

import java.util.Date;

@Entity(tableName = "actualizacion")
public class Actualizacion {
    @PrimaryKey
    private int id;

    @TypeConverters(DateConverter.class)
    private Date fechaActualizacion;

    public Actualizacion(){
    }

    public Date getFechaActualizacion(){
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion){
        this.fechaActualizacion = fechaActualizacion;
    }

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }
}